﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace Archidata.Core.Plugin.Diagram
{
    /// <summary>
    /// Define um item do diagrama
    /// </summary>
    public class DiagramViewItem: ListBoxItem
    {
        static DiagramViewItem()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(DiagramViewItem),
                new FrameworkPropertyMetadata(typeof(DiagramViewItem)));
        }

        #region Implementation

        private DiagramView ParentDiagramView
        {
            get
            {
                return ParentSelector as DiagramView;
            }
        }

        internal Selector ParentSelector
        {
            get
            {
                return ItemsControl.ItemsControlFromItemContainer(this) as Selector;
            }
        }

        #endregion

    }
}
