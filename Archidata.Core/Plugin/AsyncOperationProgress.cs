﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Archidata.Core
{
    public class AsyncOperationProgress: IProgress<OperationReport>
    {

        private static OperationReport _currentReport = new OperationReport();
        public static OperationReport CurrentReport
        {
            get { return _currentReport; }
            set { _currentReport = value; }
        }
        
        public void Report(OperationReport value)
        {
            CurrentReport.Progress = value.Progress;
            CurrentReport.OperationLabel = value.OperationLabel;
        }
    }

    public class OperationReport : INotifyPropertyChanged
    {
        public OperationReport()
        {

        }
        public OperationReport(string operationLabel, double progress)
        {
            this.OperationLabel = operationLabel;
            this.Progress = progress;
        }

        private string operationName;
        private double progress;

        public string OperationLabel
        {
            get { return operationName; }
            set
            {
                operationName = value;
                OnPropertyChanged("OperationLabel");
            }
        }
        public double Progress
        {
            get { return progress; }
            set
            {
                progress = value;
                OnPropertyChanged("Progress");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
    }
}
