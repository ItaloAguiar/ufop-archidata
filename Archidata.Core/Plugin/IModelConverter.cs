﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Archidata.Core.Plugin
{
    public interface IModelConverter
    {
        bool CanConvert { get; }
        DataModel.Database.DatabaseModel ConvertBack();

        void Convert(DataModel.Database.DatabaseModel model);
    }
}
